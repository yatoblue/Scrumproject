window.onload = illuminateGreen;

document.getElementById('stopLight').onclick = illuminateRed;
document.getElementById('slowLight').onclick = illuminateYellow;
document.getElementById('goLight').onclick = illuminateGreen;

function illuminateRed() {
  clearLights();
  document.getElementById('stopLight').style.backgroundColor = "#ff0000";
}

function illuminateYellow() {
  clearLights();
  document.getElementById('slowLight').style.backgroundColor = "#ffe500";
}

function illuminateGreen() {
  clearLights();
  document.getElementById('goLight').style.backgroundColor = "#00ff21";
}

function clearLights() {
  document.getElementById('stopLight').style.backgroundColor = "#4c0000";
  document.getElementById('slowLight').style.backgroundColor = "#756900";
  document.getElementById('goLight').style.backgroundColor = "#004709";
}
